const express = require('express');

const router = express.Router();

// ! To pass augments form the application to the route as function parameters
module.exports = () => {
  router.get('/', (request, response) => {
    // Find matching template file, with local variable
    response.render('pages/index', { pageTitle: 'Welcome' });
  });
  return router;
};
